<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();

            $table->string('title', 256);
            $table->text('description');

            $table->enum('status', ['todo', 'done'])->default('todo');
            $table->integer('priority')->default(1);

            $table->unsignedBigInteger('parent_id')
                ->nullable()
                ->default(null)
                ->references('id')
                ->on('tasks');
            $table->unsignedBigInteger('user_id')
                ->nullable()
                ->default(null)
                ->references('users')
                ->on('id');

            $table->dateTime('completed_at')->nullable();
            $table->timestamps();

            $table->softDeletes();

            $table->foreign('user_id', 'tasks_users_fk')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
