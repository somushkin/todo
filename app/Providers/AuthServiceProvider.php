<?php

namespace App\Providers;

use App\Policies\TaskPolicy;
use Illuminate\Support\Facades\Gate;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Task::class => TaskPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Gate::define('show-task', [TaskPolicy::class, 'view']);
        Gate::define('update-task', [TaskPolicy::class, 'update']);
        Gate::define('destroy-task', [TaskPolicy::class, 'delete']);
    }
}
