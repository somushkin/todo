<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $status
 * @property integer $priority
 * @property BelongsTo $parent
 * @property HasMany $children
 * @property User $user
 * @property string $completed_at
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Task extends Model
{
    use HasFactory, SoftDeletes, Filterable;
    protected $guarded = false;

    protected $hidden = [
        'deleted_at',
        'updated_at',
        'parent_id',
        'user_id'
    ];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(self::class, 'user_id');
    }

    public function isCompleted(): bool
    {
        return $this->completed_at !== null;
    }

    public static function tree($fromTask = null): Collection
    {
        if (!$fromTask) {
            return self::makeTree(self::all());
        }
        return self::makeTree($fromTask);
    }


    private static function makeTree($items): Collection
    {
        $grouped = $items->groupBy('parent_id');

        foreach ($items as $item) {
            if ($grouped->has($item->id)) {
                $item->children = $grouped[$item->id];
            }
        }

        return $items->where('parent_id', null);
    }

    public function isCanBeCompleted(): bool
    {
        return $this->children()->where('completed_at', '=', null)->count() === 0;
    }
}
