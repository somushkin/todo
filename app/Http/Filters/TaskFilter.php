<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class TaskFilter extends AbstractFilter
{
    public const TITLE = 'title';
    public const PRIORITY_FROM = 'priority_from';
    public const PRIORITY_TO = 'priority_to';
    public const STATUS = 'status';
    public const ORDER = 'order';
    public const ORDER_FIELDS = [
        'created_at',
        'completed_at',
        'priority',
    ];

    protected function getCallbacks(): array
    {
        return [
            self::TITLE => [$this, self::TITLE],
            self::PRIORITY_FROM => [$this, self::PRIORITY_FROM],
            self::PRIORITY_TO => [$this, self::PRIORITY_TO],
            self::STATUS => [$this, self::STATUS],
            self::ORDER => [$this, self::ORDER],
        ];
    }

    public function title(Builder $builder, $value)
    {
        $builder->where(self::TITLE, 'like', "%{$value}%");
    }

    public function priority_from(Builder $builder, $value)
    {
        $column = explode('_', self::PRIORITY_FROM)[0];
        $builder->where($column, '>=', $value);
    }

    public function priority_to(Builder $builder, $value)
    {
        $column = explode('_', self::PRIORITY_TO)[0];
        $builder->where($column, '<=', $value);
    }

    public function status(Builder $builder, $value)
    {
        $builder->where(self::STATUS, '=', $value);
    }

    public function order(Builder $builder, $value)
    {
        $parts = explode('_', $value);
        $direction = array_pop($parts);
        $field = implode('_', $parts);

        if (in_array($field, self::ORDER_FIELDS)) {
            $builder->orderBy($field, $direction);
        }
    }
}
