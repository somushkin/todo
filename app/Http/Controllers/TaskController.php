<?php

namespace App\Http\Controllers;

use App\Http\Filters\TaskFilter;
use App\Http\Requests\Task\FilterRequest;
use App\Http\Requests\Task\StoreRequest;
use App\Http\Requests\Task\UpdateRequest;
use App\Http\Resources\Task\TaskResource;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(FilterRequest $request): ResourceCollection
    {
        $data = $request->validated();

        if (Auth::user()) {
            $filter = app()->make(TaskFilter::class, ['queryParams' => array_filter($data)]);
            $tasks = Auth::user()->tasks()->filter($filter)->get();

            return TaskResource::collection(Task::tree($tasks));
        } else {
            abort(403, 'Not allowed');
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): TaskResource
    {
        if (Auth::user()) {
            $data = $request->validated();
            $data['user_id'] = Auth::user()->id;
            $task = Task::create($data);
            return TaskResource::make($task);
        } else {
            abort(403, 'Not allowed');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task): TaskResource
    {
        if (!Gate::allows('show-task', $task)) {
            abort(403, 'Not allowed');
        }

        return TaskResource::make($task);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Task $task): TaskResource|JsonResponse
    {
        if (!Gate::allows('update-task', $task)) {
            abort(403, 'Not allowed');
        }

        $data = $request->validated();

        if (isset($data['completed_at']) && !$task->isCanBeCompleted()) {
            return response()->json([
                'message' => 'Error: subtasks are not completed'
            ]);
        }

        $task->update($data);

        return TaskResource::make($task);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task): JsonResponse
    {
        if (!Gate::allows('destroy-task', $task)) {
            abort(403, 'Not allowed');
        }

        if ($task->isCompleted()) {
            return response()->json([
                'message' => 'Error: task is already completed'
            ]);
        }

        $task->delete();
        return response()->json([
            'message' => 'deleted'
        ]);
    }
}
